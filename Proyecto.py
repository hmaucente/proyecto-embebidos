import pandas as pd
import pyodbc
import textwrap
data = pd.read_excel (r'C:\Users\Vicho\Desktop\muestras.xlsx')
df = pd.DataFrame(data, columns= ['HumedadSuelo','TemperaturaDelSuelo','HumedadAmbiente','TemperaturaAmbiente','PHAguaDelEstanque','TemperaturaAguaDelEstanque','Precipitación'])

print(df)


server="DESKTOP-Q60189T"
database="Plantas"
conn = pyodbc.connect(
    'DRIVER={ODBC Driver 17 for SQL Server}; \
    SERVER='+ server +'; \
    DATABASE='+ database +';\
    Trusted_Connection=yes;'
)
cursor = conn.cursor()


cursor.execute('CREATE TABLE muestras (HumedadSuelo int,TemperaturaDelSuelo int,HumedadAmbiente int,TemperaturaAmbiente int,PHAguaDelEstanque float,TemperaturaAguaDelEstanque int,Precipitación int)')

for row in df.itertuples():
    cursor.execute('''
                INSERT INTO Plantas.dbo.muestras (HumedadSuelo,TemperaturaDelSuelo,HumedadAmbiente,TemperaturaAmbiente,PHAguaDelEstanque,TemperaturaAguaDelEstanque,Precipitación)
                VALUES (?,?,?,?,?,?,?)
                ''',
                row.HumedadSuelo,
                row.TemperaturaDelSuelo,
                row.HumedadAmbiente,
                row.TemperaturaAmbiente,
                row.PHAguaDelEstanque,
                row.TemperaturaAguaDelEstanque,
                row.Precipitación
                )
conn.commit()